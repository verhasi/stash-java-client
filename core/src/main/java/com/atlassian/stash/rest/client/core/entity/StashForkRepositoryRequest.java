package com.atlassian.stash.rest.client.core.entity;

import com.google.gson.JsonObject;

import javax.annotation.Nonnull;

public class StashForkRepositoryRequest {
    @Nonnull
    private final String name;
    @Nonnull
    private final String projectKey;

    public StashForkRepositoryRequest(@Nonnull final String name, @Nonnull final String projectKey) {
        this.name = name;
        this.projectKey = projectKey;
    }

    public JsonObject toJson() {
        final JsonObject req = new JsonObject();

        req.addProperty("name", name);
        req.add("project", projectToJson());

        return req;
    }

    @Nonnull
    private JsonObject projectToJson() {
        final JsonObject project = new JsonObject();
        project.addProperty("key", projectKey);
        return project;
    }
}