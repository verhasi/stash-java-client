package com.atlassian.stash.rest.client.core.entity;

import com.atlassian.stash.rest.client.api.entity.Report;
import com.atlassian.stash.rest.client.api.entity.ReportDataEntry;
import com.google.common.collect.ImmutableList;
import com.google.gson.JsonObject;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;
import java.util.List;

import static com.atlassian.stash.rest.client.core.entity.RequestUtil.addOptionalJsonProperty;
import static com.atlassian.stash.rest.client.core.entity.RequestUtil.addRequiredJsonProperty;

public class ReportRequest {
    @Nonnull
    private final String title;
    @Nonnull
    private final List<ReportDataEntry> data;
    @Nullable
    private final String details;
    @Nullable
    private final String vendor;
    @Nullable
    public final String link;
    @Nullable
    private final String logoUrl;
    @Nullable
    private final Report.Result result;

    public ReportRequest(@Nonnull Report report) {
        this(report.getTitle(),
                report.getData(),
                report.getDetails(),
                report.getVendor(),
                report.getLink(),
                report.getLogoUrl(),
                report.getResult());
    }

    private ReportRequest(@Nonnull String title,
                          @Nullable Iterable<ReportDataEntry> data,
                          @Nullable String details,
                          @Nullable String vendor,
                          @Nullable String link,
                          @Nullable String logoUrl,
                          @Nullable Report.Result result) {
        this.title = title;
        this.data = data != null ? ImmutableList.copyOf(data) : Collections.emptyList();
        this.details = details;
        this.vendor = vendor;
        this.link = link;
        this.logoUrl = logoUrl;
        this.result = result;
    }

    public JsonObject toJson() {
        final JsonObject jsonObject = new JsonObject();
        addRequiredJsonProperty(jsonObject, "title", title);
        addOptionalJsonProperty(jsonObject, "details", details);
        addOptionalJsonProperty(jsonObject, "vendor", vendor);
        addOptionalJsonProperty(jsonObject, "link", link);
        addOptionalJsonProperty(jsonObject, "logoUrl", logoUrl);
        addOptionalJsonProperty(jsonObject, "result", result);
        addOptionalJsonProperty(jsonObject, "data", data.stream()
                .map(ReportDataEntryRequest::new)
                .map(ReportDataEntryRequest::toJson)
                .collect(RequestUtil.toJsonArrayCollector()));
        return jsonObject;
    }
}
