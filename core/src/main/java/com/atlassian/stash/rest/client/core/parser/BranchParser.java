package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.entity.Branch;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import javax.annotation.Nullable;
import java.util.function.Function;

class BranchParser implements Function<JsonElement, Branch> {
    @Nullable
    @Override
    public Branch apply(@Nullable final JsonElement json) {
        if (json == null) {
            return null;
        }
        JsonObject jsonObject = json.getAsJsonObject();
        return new Branch(
                jsonObject.get("id").getAsString(),
                jsonObject.get("displayId").getAsString(),
                jsonObject.get("latestChangeset").getAsString(),
                jsonObject.get("isDefault").getAsBoolean()
        );
    }
}
