package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.entity.ReportDataEntry;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.function.Function;

import static com.atlassian.stash.rest.client.core.parser.ParserUtil.getOptionalJsonEnum;
import static com.atlassian.stash.rest.client.core.parser.ParserUtil.getRequiredJsonProperty;
import static com.atlassian.stash.rest.client.core.parser.ParserUtil.getRequiredJsonString;

public class ReportDataEntryParser implements Function<JsonElement, ReportDataEntry> {
    @Override
    public ReportDataEntry apply(JsonElement jsonElement) {
        if (jsonElement == null || !jsonElement.isJsonObject()) {
            return null;
        }
        final JsonObject jsonObject = jsonElement.getAsJsonObject();
        return new ReportDataEntry(
                getRequiredJsonString(jsonObject, "title"),
                getRequiredJsonProperty(jsonObject, "value"),
                getOptionalJsonEnum(jsonObject, "type", ReportDataEntry.Type.class));
    }
}
