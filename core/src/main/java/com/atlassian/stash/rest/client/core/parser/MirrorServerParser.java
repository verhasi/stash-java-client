package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.entity.MirrorServer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.function.Function;

public class MirrorServerParser implements Function<JsonElement, MirrorServer> {
    private static final String LINKS_ELEMENT_NAME = "links";

    @Override
    public MirrorServer apply(final JsonElement json) {
        JsonObject jsonObject = json.getAsJsonObject();

        String selfUrl = jsonObject.has(LINKS_ELEMENT_NAME)
                ? ParserUtil.getNamedLink(jsonObject.getAsJsonObject(LINKS_ELEMENT_NAME), "self")
                : null;

        JsonObject mirrorServer = jsonObject.getAsJsonObject("mirrorServer");
        return new MirrorServer(
                selfUrl,
                mirrorServer.get("id").getAsString(),
                mirrorServer.get("name").getAsString(),
                mirrorServer.get("enabled").getAsBoolean());
    }

}
