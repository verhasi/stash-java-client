package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.core.entity.Link;
import com.google.common.base.Objects;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

import static com.atlassian.stash.rest.client.core.parser.Parsers.linkParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.listParser;
import static com.google.common.base.Preconditions.checkNotNull;

public class ParserUtil {
    @Nonnull
    public static Object getRequiredJsonProperty(@Nonnull JsonObject json, @Nonnull String name) {
        return checkNotNull(getOptionalJsonProperty(json, name), String.format("Property %s not found in JSON object", name));
    }

    @Nullable
    public static Object getOptionalJsonProperty(@Nonnull JsonObject json, @Nonnull String name) {
        return getOptionalJsonProperty(json, name, null);
    }

    @Nullable
    public static Object getOptionalJsonProperty(@Nonnull JsonObject json, @Nonnull String name, @Nullable Object def) {
        final JsonElement element = json.get(name);
        if (element != null) {
            if (element.isJsonNull()) {
                return null;
            } else if (element.isJsonPrimitive()) {
                final JsonPrimitive primitive = element.getAsJsonPrimitive();
                if (primitive.isString()) {
                    return primitive.getAsString();
                } else if (primitive.isNumber()) {
                    if (primitive.getAsString().matches("^[-+]?\\d+$")) {
                        return primitive.getAsLong();
                    } else {
                        return primitive.getAsDouble();
                    }
                } else if (primitive.isBoolean()) {
                    return primitive.getAsBoolean();
                }
            }
            throw new IllegalArgumentException(String.format(
                    "Can't parse property %s of JSON object - value is of unknown type",
                    name));
        }
        return def;
    }

    @Nonnull
    public static String getRequiredJsonString(@Nonnull JsonObject json, @Nonnull String name) {
        return checkNotNull(getOptionalJsonString(json, name), String.format("Property %s not found in JSON object", name));
    }

    @Nullable
    public static String getOptionalJsonString(@Nonnull JsonObject json, @Nonnull String name) {
        return getOptionalJsonString(json, name, null);
    }

    @Nullable
    public static String getOptionalJsonString(@Nonnull JsonObject json, @Nonnull String name, @Nullable String def) {
        final JsonElement element = json.get(name);
        if (element != null) {
            if (element.isJsonNull()) {
                return null;
            } else {
                return element.getAsString();
            }
        }
        return def;
    }

    @Nonnull
    public static Integer getRequiredJsonInt(@Nonnull JsonObject json, @Nonnull String name) {
        return checkNotNull(getOptionalJsonInt(json, name), String.format("Property %s not found in JSON object", name));
    }

    @Nullable
    public static Integer getOptionalJsonInt(@Nonnull JsonObject json, @Nonnull String name) {
        return getOptionalJsonInt(json, name, null);
    }

    @Nullable
    public static Integer getOptionalJsonInt(@Nonnull JsonObject json, @Nonnull String name, @Nullable Integer def) {
        final JsonElement element = json.get(name);
        if (element != null) {
            if (element.isJsonNull()) {
                return null;
            } else {
                return element.getAsInt();
            }
        }
        return def;
    }

    @Nonnull
    public static Long getRequiredJsonLong(@Nonnull JsonObject json, @Nonnull String name) {
        return checkNotNull(getOptionalJsonLong(json, name), String.format("Property %s not found in JSON object", name));
    }

    @Nullable
    public static Long getOptionalJsonLong(@Nonnull JsonObject json, @Nonnull String name) {
        return getOptionalJsonLong(json, name, null);
    }

    @Nullable
    public static Long getOptionalJsonLong(@Nonnull JsonObject json, @Nonnull String name, @Nullable Long def) {
        final JsonElement element = json.get(name);
        if (element != null) {
            if (element.isJsonNull()) {
                return null;
            } else {
                return element.getAsLong();
            }
        }
        return def;
    }

    @Nonnull
    public static Boolean getRequiredJsonBoolean(@Nonnull JsonObject json, @Nonnull String name) {
        return checkNotNull(getOptionalJsonBoolean(json, name), String.format("Property %s not found in JSON object", name));
    }

    @Nullable
    public static Boolean getOptionalJsonBoolean(@Nonnull JsonObject json, @Nonnull String name) {
        return getOptionalJsonBoolean(json, name, null);
    }

    @Nullable
    public static Boolean getOptionalJsonBoolean(@Nonnull JsonObject json, @Nonnull String name, @Nullable Boolean def) {
        final JsonElement element = json.get(name);
        if (element != null) {
            if (element.isJsonNull()) {
                return null;
            } else {
                return element.getAsBoolean();
            }
        }
        return def;
    }

    @Nonnull
    public static <T extends Enum<T>> T getRequiredJsonEnum(@Nonnull JsonObject json, @Nonnull String name, @Nonnull Class<T> enumClass) {
        return checkNotNull(getOptionalJsonEnum(json, name, enumClass), String.format("Property %s not found in JSON object", name));
    }

    @Nullable
    public static <T extends Enum<T>> T getOptionalJsonEnum(@Nonnull JsonObject json, @Nonnull String name, @Nonnull Class<T> enumClass) {
        return getOptionalJsonEnum(json, name, enumClass, null);
    }

    @Nullable
    public static <T extends Enum<T>> T getOptionalJsonEnum(@Nonnull JsonObject json, @Nonnull String name, @Nonnull Class<T> enumClass, @Nullable T def) {
        final JsonElement element = json.get(name);
        if (element != null) {
            if (element.isJsonNull()) {
                return null;
            } else {
                return Enum.valueOf(enumClass, element.getAsString());
            }
        }
        return def;
    }

    public static Function<Link, String> linkToHref() {
        return LINK_TO_HREF;
    }

    public static Predicate<Link> isLinkName(final String name) {
        return input -> Objects.equal(input.getName(), name);
    }

    public static Predicate<Link> isHttpLink() {
        return IS_HTTP_LINK;
    }

    public static Predicate<Link> isSshLink() {
        return IS_SSH_LINK;
    }

    private static final Function<Link, String> LINK_TO_HREF = input -> input.getHref();
    private static final Predicate<Link> IS_HTTP_LINK = isLinkName("http");
    private static final Predicate<Link> IS_SSH_LINK = isLinkName("ssh");

    @Nullable
    public static String getNamedLink(@Nonnull JsonObject links, @Nonnull String relationshipName) {
        List<Link> selfLinks = listParser(linkParser("href", null)).apply(links.get(relationshipName));
        return selfLinks.stream().map(l -> linkToHref().apply(l)).findFirst().orElse(null);
    }
}
