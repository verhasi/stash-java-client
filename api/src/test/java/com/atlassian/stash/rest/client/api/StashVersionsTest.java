package com.atlassian.stash.rest.client.api;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class StashVersionsTest {
    @Test
    public void givenVersionIsOlder() throws Exception {
        assertThat(StashVersions.RENAMED_TO_BITBUCKET.givenVersionIsOlder("3999999"), is(Boolean.TRUE));
        assertThat(StashVersions.RENAMED_TO_BITBUCKET.givenVersionIsOlder("4000000"), is(Boolean.FALSE));
        assertThat(StashVersions.RENAMED_TO_BITBUCKET.givenVersionIsOlder("4000001"), is(Boolean.FALSE));
        assertThat(StashVersions.RENAMED_TO_BITBUCKET.givenVersionIsOlder("4001000"), is(Boolean.FALSE));
        assertThat(StashVersions.RENAMED_TO_BITBUCKET.givenVersionIsOlder("5001000"), is(Boolean.FALSE));
    }
}